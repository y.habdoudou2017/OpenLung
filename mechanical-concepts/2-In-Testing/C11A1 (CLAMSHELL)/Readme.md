# CONCEPT: C11A1 (CLAMSHELL)
Status: Testing

---

## Team Lead(s):
|Name|GitLab|
|---|---|
| Jonathon Kemp | @jd18 |
| | |

## Concept Overview:

### Introduction

### Function

### Mechanical Design

### Electrical

### Manufacturability

### Aesthetic

### Known Problems / Considerations

## Issue Labels:
~"C11A1/CLAMSHELL"

## Related Issues and Boards:
- Feedback Thread #0

## Other Important Links
